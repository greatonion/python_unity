from flask import Flask, request
from flask.json import jsonify
app = Flask(__name__)

cache = {}


@app.route("/yologet")
def yologet():
    return jsonify(cache['data'])


@app.route('/yoloset', methods=['POST'])
def yoloset():
    cache['data'] = request.json
    return jsonify(request.json)


if __name__ == '__main__':
    cache['foo'] = 0
    cache['data'] = ""
    app.run()

